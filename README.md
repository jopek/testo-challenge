#Quickstart

Alle Tests ausführen:

    mvn test

Service starten:

    mvn spring-boot:run




## Erstellen einer Short-URL:
**POST /**

    curl -u u1:p -H "content-type: application/json" -d '{"url": "https://bitbucket.org/jopek/testo-challenge"}' http://localhost:8080/

    {"pointToUrl":"https://bitbucket.org/jopek/testo-challenge","abbreviation":"DUs"}%

## Aufrufen einer Short-URL:
**GET /{abbreviation}**
    
    [0] % curl -v http://localhost:8080/DUs
    *   Trying ::1...
    * TCP_NODELAY set
    * Connected to localhost (::1) port 8080 (#0)
    > GET /DUs HTTP/1.1
    > Host: localhost:8080
    > User-Agent: curl/7.51.0
    > Accept: */*
    >
    < HTTP/1.1 302
    < X-Content-Type-Options: nosniff
    < X-XSS-Protection: 1; mode=block
    < Cache-Control: no-cache, no-store, max-age=0, must-revalidate
    < Pragma: no-cache
    < Expires: 0
    < X-Frame-Options: DENY
    < Location: https://bitbucket.org/jopek/testo-challenge     <-----#####
    < Content-Language: en-US
    < Content-Length: 0
    < Date: Fri, 11 Aug 2017 08:01:06 GMT
    <
    * Curl_http_done: called premature == 0
    * Connection #0 to host localhost left intact

## Aufrufstatistiken
**GET /stats** nur mit Angabe eines Usernamens und Passworts. 

Normal-User sieht nur seine Statistiken:

    curl -u u1:p http://localhost:8080/stats | jq
    
    {
      "stats": [
        {
          "shortUrl": {
            "pointToUrl": "https://bitbucket.org/jopek/testo-challenge",
            "abbreviation": "IVD"
          },
          "edits": 0,
          "uses": 0
        },
        {
          "shortUrl": {
            "pointToUrl": "https://bitbucket.org/jopek/testo-challenge",
            "abbreviation": "tK3"
          },
          "edits": 0,
          "uses": 0
        }
      ],
      "perUserStats": {
        "u1": 0
      }
    }

Admin-User sieht alle Statistiken:

    curl -u admin:pass http://localhost:8080/stats | jq

    {
      "stats": [
        {
          "shortUrl": {
            "pointToUrl": "http://google.com",
            "abbreviation": "zNf"
          },
          "edits": 0,
          "uses": 5
        },
        {
          "shortUrl": {
            "pointToUrl": "https://bitbucket.org/jopek/testo-challenge",
            "abbreviation": "IVD"
          },
          "edits": 0,
          "uses": 0
        },
        {
          "shortUrl": {
            "pointToUrl": "https://www.google.de/maps/place/Gutenbergstra%C3%9Fe+4,+10587+Berlin/@52.5161819,13.3305553,19z/data=!3m1!4b1!4m5!3m4!1s0x47a8510455215543:0x7a9a80b8ab7d3e35!8m2!3d52.5161667!4d13.3311517?hl=en",
            "abbreviation": "k5e"
          },
          "edits": 0,
          "uses": 1
        },
        {
          "shortUrl": {
            "pointToUrl": "https://bitbucket.org/jopek/testo-challenge",
            "abbreviation": "tK3"
          },
          "edits": 0,
          "uses": 3
        }
      ],
      "perUserStats": {
        "u1": 3,
        "u2": 6
      }
    }

## User anlegen
**POST /account**

    curl -v -H "content-type: application/json" -d '{"username": "alex", "password": "bobomo"}' http://localhost:8080/account
    
    > POST /account HTTP/1.1
    > Host: localhost:8080
    > User-Agent: curl/7.51.0
    > Accept: */*
    > content-type: application/json
    > Content-Length: 42
    >
    * upload completely sent off: 42 out of 42 bytes
    < HTTP/1.1 201
    < X-Content-Type-Options: nosniff
    < X-XSS-Protection: 1; mode=block
    < Cache-Control: no-cache, no-store, max-age=0, must-revalidate
    < Pragma: no-cache
    < Expires: 0
    < X-Frame-Options: DENY
    < Location: /account/alex           <----#####
    < Content-Type: application/json;charset=UTF-8
    < Transfer-Encoding: chunked
    < Date: Fri, 11 Aug 2017 09:10:53 GMT
    <
    * Curl_http_done: called premature == 0
    * Connection #0 to host localhost left intact
    {"username":"alex"}

Diese User werden automatisch angelegt (username:password):
 * admin:pass
 * u1:p
 * u2:p

## User-Accounts auflisten
**GET /account/{username}**

Jeder eingeloggte User darf andere User-Accounts sehen

    curl  http://localhost:8080/account/alex
    HTTP Status 401 - Full authentication is required to access this resource
    
    curl -u admin:pass http://localhost:8080/account/alex
    {"username":"alex"}


# Deployment

Der Service kann im DockerContainer gestartet werden.

Dazu wird das Docker-Image gebaut:

    mvn clean package docker:build

...und so gestartet:

    docker run -p 8080:8080 --name=testochallenge testochallenge 

Test:

    curl -u u1:p -H "content-type: application/json" -d '{"url": "http://google.com"}' http://192.168.99.100:8080/
    {"pointToUrl":"http://google.com","abbreviation":"zOp"}
    
`192.168.99.100` ist die VM-IP. Je nach Dockerintegration, kann diese IP auch `localhost` sein oder eine dynamisch vergebene.