package me.lxbluem.shorturl.shortener;

import me.lxbluem.shorturl.account.Account;
import me.lxbluem.shorturl.model.ShortUrl;
import me.lxbluem.shorturl.model.ShortUrlCreate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.net.MalformedURLException;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
public class ShortUrlController {

  private final ShortUrlService service;

  @Autowired
  public ShortUrlController(ShortUrlService service) {
    this.service = service;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{abbreviation}",
      produces = APPLICATION_JSON_UTF8_VALUE
  )
  public RedirectView getPointedToUrl(@PathVariable String abbreviation) {
    ShortUrl shortUrl = service.getShortUrl(abbreviation);
    return new RedirectView(shortUrl.getPointToUrl().toString());
  }

  @RequestMapping(
      method = RequestMethod.POST,
      consumes = APPLICATION_JSON_UTF8_VALUE,
      produces = APPLICATION_JSON_UTF8_VALUE
  )
  public ShortUrl createShortUrl(@RequestBody ShortUrlCreate create, Authentication authentication) throws MalformedURLException {
    Account account = mapAccount(authentication);
    return service.createShortUrl(create.getUrl(), account);
  }

  private Account mapAccount(Authentication authentication) {
    User principal = (User) authentication.getPrincipal();
    return Account.builder()
        .username(principal.getUsername())
        .build();
  }

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public void handleNotFound(Exception e) {
  }

  @ExceptionHandler(MalformedURLException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public void handleMalformedURLException(Exception e) {
  }

  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public void handleIllegalArgumentException(Exception e) {
  }
}
