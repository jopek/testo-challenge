package me.lxbluem.shorturl.shortener;

public class NotFoundException extends RuntimeException {
  public NotFoundException(String format) {
    super(format);
  }
}
