package me.lxbluem.shorturl.shortener;

import me.lxbluem.shorturl.account.Account;
import me.lxbluem.shorturl.model.ShortUrl;
import me.lxbluem.shorturl.stats.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URI;

@Service
public class ShortUrlService {
  private final AddressSpace addressSpace;
  private final ShortUrlRepo urlRepo;
  private final StatsService statsService;


  @Autowired
  public ShortUrlService(
      AddressSpace addressSpace,
      ShortUrlRepo urlRepo,
      StatsService statsService
  ) {
    this.addressSpace = addressSpace;
    this.urlRepo = urlRepo;
    this.statsService = statsService;
  }

  public ShortUrl createShortUrl(String wantedUrl, Account owner) throws MalformedURLException, IllegalArgumentException {

    ShortUrl build = ShortUrl.builder()
        .pointToUrl(URI.create(wantedUrl).toURL())
        .abbreviation(getFreeShortUrl())
        .owner(owner)
        .build();

    statsService.createStats(build);
    return urlRepo.save(build);
  }


  public ShortUrl getShortUrl(String abbreviation) {
    ShortUrl shortUrl = urlRepo.findOne(abbreviation);

    if (shortUrl == null)
      throw new NotFoundException(String.format("%s not found", abbreviation));

    statsService.updateStatsGet(shortUrl);
    return shortUrl;
  }

  private String getFreeShortUrl() {
    String rnd;
    do {
      rnd = addressSpace.newRandom();
    } while (urlRepo.exists(rnd));
    return rnd;
  }

}
