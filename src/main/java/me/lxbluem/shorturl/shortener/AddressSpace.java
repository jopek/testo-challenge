package me.lxbluem.shorturl.shortener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static java.lang.Math.floor;
import static java.lang.Math.random;

@Slf4j
@Service
public class AddressSpace {
  private final String poolCharacters;
  private final int shortUrlSize;

  @Autowired
  public AddressSpace(
      @Value("${shortUrl.address.space}") String poolCharacters,
      @Value("${shortUrl.address.length}") int shortUrlSize

  ) {
    this.poolCharacters = poolCharacters;
    this.shortUrlSize = shortUrlSize;
    logAddressSpace();
  }

  public String newRandom() {
    StringBuilder builder = new StringBuilder(shortUrlSize);
    for (int i = 0; i < shortUrlSize; i++) {
      builder.append(poolCharacters.charAt((int) floor(random() * poolCharacters.length())));
    }
    return builder.toString();
  }

  public String increaseMessageByPoolChars(String message) {
    char[] msg = message.toCharArray();
    int msgIdx = message.length() - 1;

    NextChar nextChar;
    boolean msgOverflow = false;
    do {
      nextChar = getNextChar(msg[msgIdx]);
      msg[msgIdx--] = nextChar.next;

      if (msgIdx < 0) {
        msgIdx = msg.length - 1;
        if (nextChar.overflow && message.length() < shortUrlSize) {
          char[] newArray = new char[msg.length + 1];
          System.arraycopy(msg, 0, newArray, 1, msg.length);
          newArray[0] = poolCharacters.charAt(0);
          msg = newArray;
        }
        msgOverflow = true;
      }

    } while (nextChar.overflow && !msgOverflow);

    return new String(msg);
  }

  private NextChar getNextChar(char c) {
    int poolIdx = poolCharacters.indexOf(c);
    int nextPoolIdx = (poolIdx + 1) % poolCharacters.length();
    return new NextChar(poolCharacters.charAt(nextPoolIdx), nextPoolIdx < poolIdx);
  }

  private class NextChar {
    char next;
    boolean overflow;

    public NextChar(char next, boolean overflow) {
      this.next = next;
      this.overflow = overflow;
    }

    @Override
    public String toString() {
      return "NextChar{" +
          "next=" + next +
          ", overflow=" + overflow +
          '}';
    }
  }

  private void logAddressSpace() {
    int space = 0;
    int poolSize = poolCharacters.length();

    for (int i = 1; i <= shortUrlSize; i++) {
      space += Math.pow(poolSize, i);
    }
    log.info(String.format("pool: %d  msglen: %d  possibilities: %d", poolSize, shortUrlSize, space - 1));
  }

}
