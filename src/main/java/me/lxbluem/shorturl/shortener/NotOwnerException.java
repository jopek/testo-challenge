package me.lxbluem.shorturl.shortener;

public class NotOwnerException extends RuntimeException {
  public NotOwnerException(String s) {
    super(s);
  }
}
