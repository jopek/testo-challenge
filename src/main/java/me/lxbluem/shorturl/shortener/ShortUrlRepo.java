package me.lxbluem.shorturl.shortener;


import me.lxbluem.shorturl.model.ShortUrl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.stream.Collectors.toList;

@Service
public class ShortUrlRepo implements CrudRepository<ShortUrl, String> {
  private Map<String, ShortUrl> shortUrls = new HashMap<>();

  @Override
  public ShortUrl save(ShortUrl s) {
    return Optional.ofNullable(shortUrls.put(s.getAbbreviation(), s)).orElse(s);
  }

  @Override
  public <S extends ShortUrl> Iterable<S> save(Iterable<S> entities) {
    entities.forEach(su -> shortUrls.put(su.getAbbreviation(), su));
    return entities;
  }

  @Override
  public ShortUrl findOne(String s) {
    return shortUrls.get(s);
  }

  @Override
  public boolean exists(String s) {
    return shortUrls.containsKey(s);
  }

  @Override
  public Iterable<ShortUrl> findAll() {
    return shortUrls.values();
  }

  @Override
  public Iterable<ShortUrl> findAll(Iterable<String> iterable) {
    Set<String> keys = new HashSet<>();
    iterable.forEach(keys::add);
    return keys
        .stream()
        .map(shortUrls::get)
        .collect(toList());
  }

  @Override
  public long count() {
    return shortUrls.size();
  }

  @Override
  public void delete(String s) {
    shortUrls.remove(s);
  }

  @Override
  public void delete(ShortUrl shortUrl) {
    shortUrls.remove(shortUrl.getAbbreviation());
  }

  @Override
  public void delete(Iterable<? extends ShortUrl> iterable) {
    Set<String> keys = new HashSet<>();
    iterable.forEach(e1 -> keys.add(e1.getAbbreviation()));
    keys.forEach(shortUrls::remove);
  }

  @Override
  public void deleteAll() {
    shortUrls.clear();
  }
}
