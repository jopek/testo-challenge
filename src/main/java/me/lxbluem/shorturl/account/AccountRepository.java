package me.lxbluem.shorturl.account;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.stream.Collectors.toList;

@Service
public class AccountRepository implements CrudRepository<Account, String> {

  private Map<String, Account> accounts = new HashMap<>();


  @Override
  public Account save(Account entity) {
    return Optional.ofNullable(accounts.put(entity.getUsername(), entity)).orElse(entity);
  }

  @Override
  public <S extends Account> Iterable<S> save(Iterable<S> entities) {
    entities.forEach(account -> accounts.put(account.getUsername(), account));
    return entities;
  }

  @Override
  public Account findOne(String s) {
    return accounts.get(s);
  }

  @Override
  public boolean exists(String s) {
    return accounts.containsKey(s);
  }

  @Override
  public Iterable<Account> findAll() {
    return accounts.values();
  }

  @Override
  public Iterable<Account> findAll(Iterable<String> strings) {
    Set<String> keys = new HashSet<>();
    strings.forEach(keys::add);
    return keys
        .stream()
        .map(accounts::get)
        .collect(toList());
  }

  @Override
  public long count() {
    return accounts.size();
  }

  @Override
  public void delete(String s) {
    accounts.remove(s);
  }

  @Override
  public void delete(Account entity) {
    accounts.remove(entity.getUsername());
  }

  @Override
  public void delete(Iterable<? extends Account> entities) {
    entities.forEach(this::delete);
  }

  @Override
  public void deleteAll() {
    accounts.clear();
  }
}
