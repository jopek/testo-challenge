package me.lxbluem.shorturl.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountException;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/account")
public class AccountController {

  private final AccountService accountService;

  @Autowired
  public AccountController(AccountService accountService) {
    this.accountService = accountService;
  }

  @RequestMapping(
      value = "/{username}",
      method = RequestMethod.GET,
      produces = APPLICATION_JSON_UTF8_VALUE
  )
  public Account getAccount(@PathVariable String username) {
    return accountService.get(username);
  }

  @RequestMapping(
      method = RequestMethod.POST,
      consumes = APPLICATION_JSON_UTF8_VALUE,
      produces = APPLICATION_JSON_UTF8_VALUE
  )
  public ResponseEntity<Account> createAccount(@RequestBody Account create, HttpServletRequest request) throws AccountException {
    Account account = accountService.create(create);
    String requestURI = request.getRequestURI();
    URI location = URI.create(format("%s/%s", requestURI, account.getUsername()));
    return ResponseEntity
        .created(location)
        .body(account);
  }

  @ExceptionHandler(AccountException.class)
  @ResponseStatus(value = HttpStatus.CONFLICT)
  public void handle() {
  }
}
