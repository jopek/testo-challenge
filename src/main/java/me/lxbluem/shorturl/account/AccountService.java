package me.lxbluem.shorturl.account;

import org.springframework.stereotype.Service;

import javax.security.auth.login.AccountException;

@Service
public class AccountService {
  private final AccountRepository repository;

  public AccountService(AccountRepository repository) {
    this.repository = repository;
  }

  public Account create(Account account) throws AccountException {
    Account one = repository.findOne(account.getUsername());
    if (one != null)
      throw new AccountException("Account already exists");

    return repository.save(account);
  }

  public Account get(String username) {
    return repository.findOne(username);
  }

  public Account get(Account create) {
    return get(create.getUsername());
  }
}
