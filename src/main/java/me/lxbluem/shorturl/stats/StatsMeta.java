package me.lxbluem.shorturl.stats;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Map;

@Getter
@AllArgsConstructor
public class StatsMeta {
  private List<Stats> stats;
  private Map<String, Number> perUserStats;
}
