package me.lxbluem.shorturl.stats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.function.Predicate;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(value = "/stats")
public class StatsController {

  private final StatsService service;

  @Autowired
  public StatsController(StatsService service) {
    this.service = service;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      produces = APPLICATION_JSON_UTF8_VALUE
  )
  public StatsMeta getStats(Authentication authentication) {
    boolean isAdmin = isAdmin(authentication);
    String authenticationName = authentication != null ? authentication.getName() : "";

    Predicate<Stats> predicate = stats -> isAdmin || stats.getShortUrl()
        .getOwner()
        .getUsername()
        .equals(authenticationName);

    return service.stats(predicate);
  }

  private boolean isAdmin(Authentication authentication) {
    return authentication != null && authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"));
  }

}
