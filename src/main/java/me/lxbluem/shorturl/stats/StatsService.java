package me.lxbluem.shorturl.stats;

import me.lxbluem.shorturl.model.ShortUrl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static java.util.stream.Collectors.*;

@Service
public class StatsService {
  private HashMap<String, Stats> statsHashMap = new HashMap<>();

  public void createStats(ShortUrl created) {
    Stats stats = new Stats();
    stats.setShortUrl(created);
    statsHashMap.put(created.getAbbreviation(), stats);
  }

  public void updateStatsGet(ShortUrl shortUrl) {
    Stats stats = statsHashMap.get(shortUrl.getAbbreviation());
    stats.getUses().incrementAndGet();
  }

  public void updateStatsEdit(ShortUrl shortUrl) {
    Stats stats = statsHashMap.get(shortUrl.getAbbreviation());
    stats.getEdits().incrementAndGet();
  }

  public StatsMeta stats(Predicate<Stats> filter) {
    List<Stats> statsList = getListOfStats(filter);
    Map<String, Number> meta = getMetaStats(filter);
    return new StatsMeta(statsList, meta);
  }

  private List<Stats> getListOfStats(Predicate<Stats> filter) {
    return statsHashMap.values()
        .stream()
        .filter(filter)
        .collect(toList());
  }

  private Map<String, Number> getMetaStats(Predicate<Stats> filter) {
    return statsHashMap.values()
        .stream()
        .filter(filter)
        .collect(groupingBy(o -> o.getShortUrl().getOwner()))
        .entrySet()
        .stream()
        .collect(toMap(
            entry -> entry.getKey().getUsername(),
            entry -> (Number) entry.getValue()
                .stream()
                .mapToInt(stats -> stats.getUses().get())
                .sum()
        ));
  }
}
