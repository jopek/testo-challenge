package me.lxbluem.shorturl.stats;

import lombok.Getter;
import lombok.Setter;
import me.lxbluem.shorturl.model.ShortUrl;

import java.util.concurrent.atomic.AtomicInteger;

@Getter
public class Stats {
  @Setter
  private ShortUrl shortUrl;
  private AtomicInteger edits = new AtomicInteger();
  private AtomicInteger uses = new AtomicInteger();
}
