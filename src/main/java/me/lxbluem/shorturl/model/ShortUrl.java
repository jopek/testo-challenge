package me.lxbluem.shorturl.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import me.lxbluem.shorturl.account.Account;

import java.io.Serializable;
import java.net.URL;

@Builder(toBuilder = true)
@EqualsAndHashCode
@Getter
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ShortUrl implements Serializable {
  private final URL pointToUrl;
  private final String abbreviation;

  @JsonIgnore
  private final Account owner;
}
