package me.lxbluem.shorturl;

import me.lxbluem.shorturl.account.Account;
import me.lxbluem.shorturl.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.List;

import static java.util.Arrays.asList;

@SpringBootApplication
public class TestoChallengeApplication {

  private final AccountRepository accountRepository;

  @Autowired
  public TestoChallengeApplication(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @PostConstruct
  private void populateAccountRepository() {
    List<Account> accounts = asList(
        new Account("admin", "pass"),
        new Account("u1", "p"),
        new Account("u2", "p")
    );
    accountRepository.save(accounts);
  }

  public static void main(String[] args) {
    SpringApplication.run(TestoChallengeApplication.class, args);
  }

}
