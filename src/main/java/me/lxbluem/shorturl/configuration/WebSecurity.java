package me.lxbluem.shorturl.configuration;

import me.lxbluem.shorturl.account.Account;
import me.lxbluem.shorturl.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
  private AuthenticationEntryPoint authEntryPoint;
  private AccountRepository accountRepository;

  @Autowired
  public WebSecurity(AccountRepository accountRepository, AuthenticationEntryPoint authEntryPoint) {
    this.accountRepository = accountRepository;
    this.authEntryPoint = authEntryPoint;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().authorizeRequests()
        .antMatchers(HttpMethod.GET, "/stats").authenticated()
        .antMatchers(HttpMethod.POST, "/account").permitAll()
        .antMatchers(HttpMethod.POST, "/*").authenticated()
        .antMatchers("/*").permitAll()
        .anyRequest().authenticated()
        .and()
        .httpBasic().authenticationEntryPoint(authEntryPoint);
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService());
  }

  @Bean
  public UserDetailsService userDetailsService() {
    return username -> {
      Account account = accountRepository.findOne(username);
      if (account != null) {
        return new User(account.getUsername(),
            account.getPassword(),
            true,
            true,
            true,
            true,
            AuthorityUtils.createAuthorityList(getRoles(account)));
      }
      throw new UsernameNotFoundException("could not find the user '" + username + "'");
    };
  }

  private String[] getRoles(Account account) {
    List<String> roles = new ArrayList<>();
    roles.add("USER");

    if ("admin".equals(account.getUsername()))
      roles.add("ADMIN");

    return roles.toArray(new String[roles.size()]);
  }
}