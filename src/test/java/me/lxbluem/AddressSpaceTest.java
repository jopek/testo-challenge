package me.lxbluem;

import me.lxbluem.shorturl.shortener.AddressSpace;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddressSpaceTest {

  @Test
  public void increase_without_overflow() throws Exception {
    AddressSpace abc = new AddressSpace("ABC", 2);

    assertEquals("AB", abc.increaseMessageByPoolChars("AA"));
    assertEquals("CB", abc.increaseMessageByPoolChars("CA"));
    assertEquals("AC", abc.increaseMessageByPoolChars("AB"));
  }

  @Test
  public void increase_with_overflow() throws Exception {
    AddressSpace abc = new AddressSpace("ABC", 2);

    assertEquals("BA", abc.increaseMessageByPoolChars("AC"));
    assertEquals("AA", abc.increaseMessageByPoolChars("CC"));
  }

  @Test
  public void increase_with_overflow_enlarge_message() throws Exception {
    AddressSpace abc = new AddressSpace("ABC", 3);

    assertEquals("AAA", abc.increaseMessageByPoolChars("CC"));
  }

}