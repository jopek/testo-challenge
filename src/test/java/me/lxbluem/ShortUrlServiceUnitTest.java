package me.lxbluem;

import me.lxbluem.shorturl.account.Account;
import me.lxbluem.shorturl.model.ShortUrl;
import me.lxbluem.shorturl.shortener.AddressSpace;
import me.lxbluem.shorturl.shortener.ShortUrlRepo;
import me.lxbluem.shorturl.shortener.ShortUrlService;
import me.lxbluem.shorturl.stats.StatsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.MalformedURLException;
import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ShortUrlServiceUnitTest {

  @Captor
  private ArgumentCaptor<ShortUrl> urlArgumentCaptor;

  @Mock
  private AddressSpace addressSpace;

  @Mock
  private StatsService statsService;

  @Mock
  private ShortUrlRepo repo;

  @InjectMocks
  private ShortUrlService shortUrlService;

  private Account owner;

  @Before
  public void setUp() throws Exception {
    owner = Account.builder().username("dude").password("asd").build();

    when(addressSpace.newRandom()).thenReturn("abc", "acc", "bab");
  }

  @Test
  public void create_save_not_existant_shortUrl() throws Exception {
    Account owner = Account.builder().username("dude").password("asd").build();
    String wantedUrl = "http://www.google.com/lalal";
    ShortUrl shortUrl = ShortUrl.builder()
        .pointToUrl(URI.create(wantedUrl).toURL())
        .abbreviation("abc")
        .owner(owner)
        .build();

    when(repo.exists("abc")).thenReturn(false);
    when(repo.save(any(ShortUrl.class))).thenReturn(shortUrl);

    ShortUrl returnedShortUrl = shortUrlService.createShortUrl(wantedUrl, owner);

    verify(statsService).createStats(returnedShortUrl);
    verify(addressSpace).newRandom();
    verify(repo).exists("abc");


    assertSame(shortUrl, returnedShortUrl);
  }

  @Test
  public void create_save_existing_shortUrl() throws Exception {
    String wantedUrl = "http://www.google.com/lalal";

    when(repo.exists("abc")).thenReturn(true);
    when(repo.exists("acc")).thenReturn(false);

    ShortUrl expectedShortUrl = ShortUrl.builder()
        .pointToUrl(URI.create(wantedUrl).toURL())
        .abbreviation("acc")
        .owner(owner)
        .build();

    when(repo.save(any(ShortUrl.class))).thenReturn(expectedShortUrl);

    ShortUrl returnedShortUrl = shortUrlService.createShortUrl(wantedUrl, owner);
    assertSame(expectedShortUrl, returnedShortUrl);

    verify(statsService).createStats(returnedShortUrl);
    verify(addressSpace, times(2)).newRandom();
    verify(repo).exists("abc");
    verify(repo).exists("acc");
    verify(repo).save(urlArgumentCaptor.capture());

    assertEquals(expectedShortUrl, urlArgumentCaptor.getValue());
  }

  @Test()
  public void no_exception() throws Exception {
    when(repo.save(any(ShortUrl.class))).thenAnswer(invocationOnMock -> invocationOnMock.getArguments()[0]);

    ShortUrl shortUrl = shortUrlService.createShortUrl("http://lalal/path?query=wot#!v=param:pssh", owner);

    assertEquals("abc", shortUrl.getAbbreviation());
  }

  @Test(expected = IllegalArgumentException.class)
  public void illegal_url_throws_exception() throws Exception {
    shortUrlService.createShortUrl("lalal", owner);
  }

  @Test(expected = MalformedURLException.class)
  public void malformed_url_throws_exception() throws Exception {
    shortUrlService.createShortUrl("pp:/lalal", owner);
  }
}