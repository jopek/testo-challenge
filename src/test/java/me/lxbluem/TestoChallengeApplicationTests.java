package me.lxbluem;

import me.lxbluem.shorturl.TestoChallengeApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = TestoChallengeApplication.class)
public class TestoChallengeApplicationTests {

	@Test
	public void contextLoads() {
	}

}
