package me.lxbluem;

import me.lxbluem.shorturl.account.Account;
import me.lxbluem.shorturl.model.ShortUrl;
import me.lxbluem.shorturl.shortener.AddressSpace;
import me.lxbluem.shorturl.shortener.ShortUrlRepo;
import me.lxbluem.shorturl.shortener.ShortUrlService;
import me.lxbluem.shorturl.stats.Stats;
import me.lxbluem.shorturl.stats.StatsService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ShortUrlServiceTest {
  private ShortUrlService service;
  private ShortUrlRepo repo;
  private StatsService stats;

  @Before
  public void setUp() throws Exception {
    AddressSpace space = new AddressSpace("123abc", 4);
    repo = new ShortUrlRepo();
    stats = new StatsService();
    service = new ShortUrlService(space, repo, stats);
  }

  @Test
  public void createShortUrl() throws Exception {
    ShortUrl shortUrl = service.createShortUrl("http://localhost:8080/playground", Account.builder().build());

    System.out.printf("created %s\n", shortUrl);

    String representation = shortUrl.getAbbreviation();
    assertTrue(repo.exists(representation));
    assertEquals(1, repo.count());

    List<Stats> statsList = this.stats.stats(s -> true).getStats();
    assertEquals(1, statsList.size());
  }

  @Test
  public void create_duplicate_ShortUrl() throws Exception {
    List<ShortUrl> shortUrls = new ArrayList<>();
    shortUrls.add(service.createShortUrl("http://localhost:8080/playground", Account.builder().username("ppp").build()));
    shortUrls.add(service.createShortUrl("http://localhost:8080/playground", Account.builder().username("xxx").build()));

    System.out.printf("created %s\n", shortUrls);

    assertTrue(repo.exists(shortUrls.get(0).getAbbreviation()));
    assertTrue(repo.exists(shortUrls.get(1).getAbbreviation()));
    assertEquals(2, repo.count());

    List<Stats> statsList = this.stats.stats(s -> true).getStats();
    Map<String, Number> perUserStats = this.stats.stats(s -> true).getPerUserStats();
    assertEquals(2, statsList.size());
    assertEquals(2, perUserStats.size());

    // aggregated uses per user
    assertTrue(perUserStats.containsKey("ppp"));
    assertEquals(0, perUserStats.get("ppp").intValue());

    assertTrue(perUserStats.containsKey("xxx"));
    assertEquals(0, perUserStats.get("xxx").intValue());

  }
}